﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public partial class LogInForm : Form
    {
        Library l;
        LogInForm logInForm;
        LibraryForm libraryForm;
        EmployeeForm employeeForm;
        public LogInForm(Library library)
        {
            InitializeComponent();
            l = library;
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            logInForm = new LogInForm(l);
            if (!String.IsNullOrEmpty(tbxName.Text) && !String.IsNullOrEmpty(tbxPassword.Text))
            {
                if (l.GetEmployee(tbxName.Text, tbxPassword.Text) != null)
                {
                    l.GetEmployee(tbxName.Text, tbxPassword.Text);

                    employeeForm = new EmployeeForm(l);
                    this.Close();
                    employeeForm.Show();
                    logInForm.Hide();
                }
                else if (l.GetCustomer(tbxName.Text, tbxPassword.Text) != null)
                {
                    l.GetCustomer(tbxName.Text, tbxPassword.Text);
                    MessageBox.Show("Logged in succesfully as a customer");
                    libraryForm = new LibraryForm(l);
                    this.Close();
                    libraryForm.Show();
                    logInForm.Hide();
                }
                else
                {
                    MessageBox.Show("Wrong password or username");
                }
            }
            else
            {
                MessageBox.Show("Please provide a name or a password");
            }
        }
    }
}
