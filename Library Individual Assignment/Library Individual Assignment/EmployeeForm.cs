﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public partial class EmployeeForm : Form
    {
        Library l;
        Random random = new Random();
        public EmployeeForm(Library library)
        {
            InitializeComponent();
            l = library;
            foreach (GenreType genre in Enum.GetValues(typeof(GenreType)))
            {
                cmbGenre.Items.Add(genre);
            }

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbTitles.Items.Add(b);
                }
            }
        }

        private void ManagerForm_Load(object sender, EventArgs e)
        {

        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            lbTitles.Items.Clear();

            String r = random.Next(0, 1000000).ToString("D6");
            r += random.Next(0, 10000000).ToString("D7");

            if (!String.IsNullOrEmpty(tbxTitle.Text) && !String.IsNullOrEmpty(tbxPages.Text) &&
                !String.IsNullOrEmpty(tbxDescription.Text) && !String.IsNullOrEmpty(tbxAuthor.Text) && cmbGenre.SelectedIndex > -1
                && dtpPublicationDate.Value != null && !String.IsNullOrEmpty(tbxCopies.Text))
            {
                l.AddBook(r, tbxTitle.Text, tbxAuthor.Text,
                    (GenreType)cmbGenre.SelectedIndex, dtpPublicationDate.Value.Date, Convert.ToInt32(tbxPages.Text),
                    tbxDescription.Text, Convert.ToInt32(tbxCopies.Text));

                tbxTitle.Clear();
                tbxPages.Clear();
                tbxDescription.Clear();
                tbxAuthor.Clear();
                cmbGenre.SelectedIndex = -1;
                tbxCopies.Clear();

                foreach (Book b in l.GetBooks())
                {
                    lbTitles.Items.Add(b);
                }

                MessageBox.Show("Book added succesfully");
            }
            else
            {
                MessageBox.Show("Please fill out everything in order to add a book");
            }


        }

        private void btnRemoveBook_Click(object sender, EventArgs e)
        {
            Book selectedBook = (Book)lbTitles.SelectedItem;

            l.RemoveBook(selectedBook);

            lbTitles.Items.Clear();

            foreach (Book b in l.GetBooks())
            {
                lbTitles.Items.Add(b);
            }

            MessageBox.Show("Book removed succesfully");
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            if (l.GetEmployee() != null)
            {
                foreach (Employee emp in l.GetEmployees())
                {
                    emp.IsLoggedIn = false;
                }
                this.Close();
            }
        }

        private void btnAddMoreCopies_Click(object sender, EventArgs e)
        {
            if (lbTitles.SelectedIndex > -1)
            {
                if (!String.IsNullOrEmpty(tbxNumberOfCopies.Text))
                {
                    Book selectedBook = (Book)lbTitles.SelectedItem;

                    selectedBook.Copies += Convert.ToInt32(tbxNumberOfCopies.Text);
                    MessageBox.Show("Copies added succesfully");

                }
                else
                {
                    MessageBox.Show("Please add a number of copies");
                }

                tbxNumberOfCopies.Clear();
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            lbTitles.Items.Clear();

            foreach (Book b in l.FilterBooks(tbxFilterTitle.Text, tbxFilterAuthor.Text, (GenreType)cmbGenres.SelectedIndex))
            {
                lbTitles.Items.Add(b);
            }

            tbxFilterAuthor.Clear();
            tbxFilterTitle.Clear();
            cmbGenres.SelectedIndex = -1;
        }

        private void tbxShowAllBooks_Click(object sender, EventArgs e)
        {
            lbTitles.Items.Clear();

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbTitles.Items.Add(b);
                }
            }
        }

        private void lbTitles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbTitles.SelectedIndex > -1)
            {

                Book selectedBook = (Book)lbTitles.SelectedItem;

                tbxShowAuthor.Text = selectedBook.Author;
                tbxShowGenre.Text = selectedBook.Genre.ToString();
                tbxShowCopies.Text = selectedBook.Copies.ToString();
            }
        }
    }
}
