﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public class Customer
    {
        private string name;
        private string password;
        private List<Book> borrowedBooks;
        private int id;
        private static int idSeeder = 00001;
        private bool isLoggedIn;
        private string value;
        public Customer(string name, string password)
        {
            this.name = name;
            this.password = password;
            borrowedBooks = new List<Book>();
            isLoggedIn = false;
            this.id = idSeeder++;
            value = String.Format("{0:D5}", id);
        }
        public string Name { get { return name; } }
        public string Password { get { return password; } }
        public bool IsLoggedIn { get { return isLoggedIn; } set { isLoggedIn = value; } }
        public static int IdSeeder { get { return idSeeder; } set { idSeeder = value; } }
        public int Id { get { return id; } set { id = value; } }
        public List<Book> BorrowedBooks { get { return borrowedBooks; } set { borrowedBooks = value; } }

        public string GetInfo()
        {
            return $"{this.name} (no.{this.value})";
        }

        public void BorrowBook(Book book)
        {
            if (book.Copies > 0)
            {
                borrowedBooks.Add(book);
                book.AddBorrowers(ToString());
                book.BorrowedDate = DateTime.Now.Date;
                book.Copies -= 1;
            }
            else
            {
                book.Borrowed = true;
            }
        }

        public void ReturnBook(Book book)
        {
            borrowedBooks.Remove(book);
            book.ReturnDate = DateTime.Now.Date;
            book.CurrentBorrowers = "";
            book.Copies += 1;

            if (book.Copies > 0)
            {
                book.Borrowed = false;
            }
        }

        public List<Book> GetBorrowedBooks()
        {
            return borrowedBooks;
        }

        public override string ToString()
        {
            return $"{Name} (no{this.value})";
        }
    }
}
