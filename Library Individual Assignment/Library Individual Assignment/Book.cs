using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


namespace Library_Individual_Assignment
{
    [Serializable]
    public class Book
    {
        private string isbn13;
        private string title;
        private string author;
        private GenreType genre;
        private DateTime publicationDate;
        private int pages;
        private string description;
        private int copies;
        private bool borrowed = false;
        private string currentBorrowers;
        private string pastBorrower;
        private DateTime borrowedDate;
        private DateTime returnDate;
        private List<ReturnData> returnData;
        private List<string> borrowers;

        public Book(string isbn13, string title, string author, GenreType genre, DateTime publicationDate, int pages, string description, int copies)
        {
            this.isbn13 = isbn13;
            this.title = title;
            this.author = author;
            this.genre = genre;
            this.publicationDate = publicationDate;
            this.pages = pages;
            this.description = description;
            this.copies = copies;
            this.borrowed = false;
            returnData = new List<ReturnData>();
            borrowers = new List<string>();
        }
        public string Title { get { return title; } set { title = value; } }
        public string Author { get { return author; } }
        public GenreType Genre { get { return genre; } }
        public DateTime PublicationDate { get { return publicationDate; } }
        public int Pages { get { return pages; } }
        public string Description { get { return description; } }
        public bool Borrowed { get { return borrowed; } set { borrowed = value; } }
        public string Isbn13 { get { return isbn13; } }
        public int Copies { get { return copies; } set { copies = value; } }
        public string CurrentBorrowers { get { return currentBorrowers; } set { currentBorrowers = value; } }
        public string PastBorrower { get { return pastBorrower; } set { pastBorrower = value; } }
        public DateTime BorrowedDate { get { return borrowedDate; } set { borrowedDate = value; } }
        public DateTime ReturnDate { get { return returnDate; } set { returnDate = value; } }
        public List<ReturnData> ReturnData { get { return returnData; } set { returnData = value; } }
        public List<string> Borrowers { get { return borrowers; } set { borrowers = value; } }

        public void AddReturnData(string pastBorrower, DateTime borrowedDate, DateTime returnDate)
        {
            returnData.Add(new ReturnData(PastBorrower, BorrowedDate, ReturnDate));
        }

        public ReturnData GetReturnData(string borrower)
        {
            foreach (ReturnData r in returnData)
            {
                if (borrower == r.Borrower)
                {
                    return r;
                }
            }
            return null;
        }

        public ReturnData[] GetAllReturnData()
        {
            return returnData.ToArray();
        }

        public void AddBorrowers(string customer)
        {
            borrowers.Add(customer);
        }
        
        public void RemoveBorrowers(string customer)
        {
            borrowers.Remove(customer);
        }

        public string[] GetBorrowers()
        {
            return borrowers.ToArray();
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
