﻿namespace Library_Individual_Assignment
{
    partial class LibraryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBookTitle = new System.Windows.Forms.ListBox();
            this.tbxDescription = new System.Windows.Forms.TextBox();
            this.tbxIsbn13 = new System.Windows.Forms.TextBox();
            this.tbxPages = new System.Windows.Forms.TextBox();
            this.tbxPublicationDate = new System.Windows.Forms.TextBox();
            this.tbxGenre = new System.Windows.Forms.TextBox();
            this.tbxAuthor = new System.Windows.Forms.TextBox();
            this.lblReturnDate = new System.Windows.Forms.Label();
            this.lblBorrowDate = new System.Windows.Forms.Label();
            this.lblPerson = new System.Windows.Forms.Label();
            this.lblHistoryOfBorrowers = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblCurrentBorrower = new System.Windows.Forms.Label();
            this.lblIsbn13 = new System.Windows.Forms.Label();
            this.lblPages = new System.Windows.Forms.Label();
            this.lblPublicationDate = new System.Windows.Forms.Label();
            this.lblGenre = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnBorrowBook = new System.Windows.Forms.Button();
            this.btnReturnBook = new System.Windows.Forms.Button();
            this.btnShowBooks = new System.Windows.Forms.Button();
            this.lblFilterGenre = new System.Windows.Forms.Label();
            this.lblFilterAuthor = new System.Windows.Forms.Label();
            this.lblFilterTitle = new System.Windows.Forms.Label();
            this.cmbGenres = new System.Windows.Forms.ComboBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.tbxFilterAuthor = new System.Windows.Forms.TextBox();
            this.tbxTitle = new System.Windows.Forms.TextBox();
            this.lblFilters = new System.Windows.Forms.Label();
            this.btnRemoveFilters = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lbCustomer = new System.Windows.Forms.ListBox();
            this.lbBorrowDate = new System.Windows.Forms.ListBox();
            this.lbReturnDate = new System.Windows.Forms.ListBox();
            this.tbxCopies = new System.Windows.Forms.TextBox();
            this.lblCopies = new System.Windows.Forms.Label();
            this.lbCurrentBorrowers = new System.Windows.Forms.ListBox();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbBookTitle
            // 
            this.lbBookTitle.FormattingEnabled = true;
            this.lbBookTitle.ItemHeight = 16;
            this.lbBookTitle.Location = new System.Drawing.Point(12, 44);
            this.lbBookTitle.Name = "lbBookTitle";
            this.lbBookTitle.Size = new System.Drawing.Size(363, 452);
            this.lbBookTitle.TabIndex = 1;
            this.lbBookTitle.SelectedIndexChanged += new System.EventHandler(this.lbBookTitle_SelectedIndexChanged);
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(945, 32);
            this.tbxDescription.Multiline = true;
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.ReadOnly = true;
            this.tbxDescription.Size = new System.Drawing.Size(265, 461);
            this.tbxDescription.TabIndex = 3;
            // 
            // tbxIsbn13
            // 
            this.tbxIsbn13.Location = new System.Drawing.Point(429, 192);
            this.tbxIsbn13.Multiline = true;
            this.tbxIsbn13.Name = "tbxIsbn13";
            this.tbxIsbn13.ReadOnly = true;
            this.tbxIsbn13.Size = new System.Drawing.Size(209, 33);
            this.tbxIsbn13.TabIndex = 4;
            // 
            // tbxPages
            // 
            this.tbxPages.Location = new System.Drawing.Point(429, 117);
            this.tbxPages.Multiline = true;
            this.tbxPages.Name = "tbxPages";
            this.tbxPages.ReadOnly = true;
            this.tbxPages.Size = new System.Drawing.Size(209, 34);
            this.tbxPages.TabIndex = 5;
            // 
            // tbxPublicationDate
            // 
            this.tbxPublicationDate.Location = new System.Drawing.Point(684, 117);
            this.tbxPublicationDate.Multiline = true;
            this.tbxPublicationDate.Name = "tbxPublicationDate";
            this.tbxPublicationDate.ReadOnly = true;
            this.tbxPublicationDate.Size = new System.Drawing.Size(190, 34);
            this.tbxPublicationDate.TabIndex = 6;
            // 
            // tbxGenre
            // 
            this.tbxGenre.Location = new System.Drawing.Point(684, 45);
            this.tbxGenre.Multiline = true;
            this.tbxGenre.Name = "tbxGenre";
            this.tbxGenre.ReadOnly = true;
            this.tbxGenre.Size = new System.Drawing.Size(190, 35);
            this.tbxGenre.TabIndex = 7;
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(429, 45);
            this.tbxAuthor.Multiline = true;
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.ReadOnly = true;
            this.tbxAuthor.Size = new System.Drawing.Size(209, 35);
            this.tbxAuthor.TabIndex = 8;
            // 
            // lblReturnDate
            // 
            this.lblReturnDate.AutoSize = true;
            this.lblReturnDate.Location = new System.Drawing.Point(796, 361);
            this.lblReturnDate.Name = "lblReturnDate";
            this.lblReturnDate.Size = new System.Drawing.Size(78, 16);
            this.lblReturnDate.TabIndex = 15;
            this.lblReturnDate.Text = "Return Date";
            // 
            // lblBorrowDate
            // 
            this.lblBorrowDate.AutoSize = true;
            this.lblBorrowDate.Location = new System.Drawing.Point(587, 361);
            this.lblBorrowDate.Name = "lblBorrowDate";
            this.lblBorrowDate.Size = new System.Drawing.Size(81, 16);
            this.lblBorrowDate.TabIndex = 16;
            this.lblBorrowDate.Text = "Borrow Date";
            // 
            // lblPerson
            // 
            this.lblPerson.AutoSize = true;
            this.lblPerson.Location = new System.Drawing.Point(381, 361);
            this.lblPerson.Name = "lblPerson";
            this.lblPerson.Size = new System.Drawing.Size(50, 16);
            this.lblPerson.TabIndex = 17;
            this.lblPerson.Text = "Person";
            // 
            // lblHistoryOfBorrowers
            // 
            this.lblHistoryOfBorrowers.AutoSize = true;
            this.lblHistoryOfBorrowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHistoryOfBorrowers.Location = new System.Drawing.Point(531, 303);
            this.lblHistoryOfBorrowers.Name = "lblHistoryOfBorrowers";
            this.lblHistoryOfBorrowers.Size = new System.Drawing.Size(268, 32);
            this.lblHistoryOfBorrowers.TabIndex = 18;
            this.lblHistoryOfBorrowers.Text = "History of Borrowers";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(942, 9);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(75, 16);
            this.lblDescription.TabIndex = 19;
            this.lblDescription.Text = "Description";
            // 
            // lblCurrentBorrower
            // 
            this.lblCurrentBorrower.AutoSize = true;
            this.lblCurrentBorrower.Location = new System.Drawing.Point(681, 173);
            this.lblCurrentBorrower.Name = "lblCurrentBorrower";
            this.lblCurrentBorrower.Size = new System.Drawing.Size(106, 16);
            this.lblCurrentBorrower.TabIndex = 20;
            this.lblCurrentBorrower.Text = "Current Borrower";
            // 
            // lblIsbn13
            // 
            this.lblIsbn13.AutoSize = true;
            this.lblIsbn13.Location = new System.Drawing.Point(426, 173);
            this.lblIsbn13.Name = "lblIsbn13";
            this.lblIsbn13.Size = new System.Drawing.Size(52, 16);
            this.lblIsbn13.TabIndex = 21;
            this.lblIsbn13.Text = "ISBN13";
            // 
            // lblPages
            // 
            this.lblPages.AutoSize = true;
            this.lblPages.Location = new System.Drawing.Point(426, 98);
            this.lblPages.Name = "lblPages";
            this.lblPages.Size = new System.Drawing.Size(47, 16);
            this.lblPages.TabIndex = 22;
            this.lblPages.Text = "Pages";
            // 
            // lblPublicationDate
            // 
            this.lblPublicationDate.AutoSize = true;
            this.lblPublicationDate.Location = new System.Drawing.Point(681, 98);
            this.lblPublicationDate.Name = "lblPublicationDate";
            this.lblPublicationDate.Size = new System.Drawing.Size(105, 16);
            this.lblPublicationDate.TabIndex = 23;
            this.lblPublicationDate.Text = "Publication Date";
            // 
            // lblGenre
            // 
            this.lblGenre.AutoSize = true;
            this.lblGenre.Location = new System.Drawing.Point(681, 20);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(44, 16);
            this.lblGenre.TabIndex = 24;
            this.lblGenre.Text = "Genre";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(426, 20);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(45, 16);
            this.lblAuthor.TabIndex = 25;
            this.lblAuthor.Text = "Author";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(12, 19);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 16);
            this.lblTitle.TabIndex = 26;
            this.lblTitle.Text = "Titles";
            // 
            // btnBorrowBook
            // 
            this.btnBorrowBook.Location = new System.Drawing.Point(684, 522);
            this.btnBorrowBook.Name = "btnBorrowBook";
            this.btnBorrowBook.Size = new System.Drawing.Size(136, 102);
            this.btnBorrowBook.TabIndex = 27;
            this.btnBorrowBook.Text = "Borrow book";
            this.btnBorrowBook.UseVisualStyleBackColor = true;
            this.btnBorrowBook.Click += new System.EventHandler(this.btnBorrowBook_Click);
            // 
            // btnReturnBook
            // 
            this.btnReturnBook.Location = new System.Drawing.Point(833, 522);
            this.btnReturnBook.Name = "btnReturnBook";
            this.btnReturnBook.Size = new System.Drawing.Size(119, 102);
            this.btnReturnBook.TabIndex = 28;
            this.btnReturnBook.Text = "Return Book";
            this.btnReturnBook.UseVisualStyleBackColor = true;
            this.btnReturnBook.Click += new System.EventHandler(this.btnReturnBook_Click);
            // 
            // btnShowBooks
            // 
            this.btnShowBooks.Location = new System.Drawing.Point(532, 522);
            this.btnShowBooks.Name = "btnShowBooks";
            this.btnShowBooks.Size = new System.Drawing.Size(140, 102);
            this.btnShowBooks.TabIndex = 29;
            this.btnShowBooks.Text = "Show Borrowed Books";
            this.btnShowBooks.UseVisualStyleBackColor = true;
            this.btnShowBooks.Click += new System.EventHandler(this.btnShowBooks_Click);
            // 
            // lblFilterGenre
            // 
            this.lblFilterGenre.AutoSize = true;
            this.lblFilterGenre.Location = new System.Drawing.Point(273, 533);
            this.lblFilterGenre.Name = "lblFilterGenre";
            this.lblFilterGenre.Size = new System.Drawing.Size(44, 16);
            this.lblFilterGenre.TabIndex = 30;
            this.lblFilterGenre.Text = "Genre";
            // 
            // lblFilterAuthor
            // 
            this.lblFilterAuthor.AutoSize = true;
            this.lblFilterAuthor.Location = new System.Drawing.Point(132, 533);
            this.lblFilterAuthor.Name = "lblFilterAuthor";
            this.lblFilterAuthor.Size = new System.Drawing.Size(45, 16);
            this.lblFilterAuthor.TabIndex = 31;
            this.lblFilterAuthor.Text = "Author";
            // 
            // lblFilterTitle
            // 
            this.lblFilterTitle.AutoSize = true;
            this.lblFilterTitle.Location = new System.Drawing.Point(12, 533);
            this.lblFilterTitle.Name = "lblFilterTitle";
            this.lblFilterTitle.Size = new System.Drawing.Size(33, 16);
            this.lblFilterTitle.TabIndex = 32;
            this.lblFilterTitle.Text = "Title";
            // 
            // cmbGenres
            // 
            this.cmbGenres.FormattingEnabled = true;
            this.cmbGenres.Location = new System.Drawing.Point(265, 562);
            this.cmbGenres.Name = "cmbGenres";
            this.cmbGenres.Size = new System.Drawing.Size(121, 24);
            this.cmbGenres.TabIndex = 33;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(135, 590);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(116, 34);
            this.btnFilter.TabIndex = 34;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // tbxFilterAuthor
            // 
            this.tbxFilterAuthor.Location = new System.Drawing.Point(131, 562);
            this.tbxFilterAuthor.Name = "tbxFilterAuthor";
            this.tbxFilterAuthor.Size = new System.Drawing.Size(120, 22);
            this.tbxFilterAuthor.TabIndex = 35;
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(15, 562);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(100, 22);
            this.tbxTitle.TabIndex = 36;
            // 
            // lblFilters
            // 
            this.lblFilters.AutoSize = true;
            this.lblFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilters.Location = new System.Drawing.Point(146, 499);
            this.lblFilters.Name = "lblFilters";
            this.lblFilters.Size = new System.Drawing.Size(64, 25);
            this.lblFilters.TabIndex = 37;
            this.lblFilters.Text = "Filters";
            // 
            // btnRemoveFilters
            // 
            this.btnRemoveFilters.Location = new System.Drawing.Point(392, 522);
            this.btnRemoveFilters.Name = "btnRemoveFilters";
            this.btnRemoveFilters.Size = new System.Drawing.Size(121, 102);
            this.btnRemoveFilters.TabIndex = 38;
            this.btnRemoveFilters.Text = "Remove Filters";
            this.btnRemoveFilters.UseVisualStyleBackColor = true;
            this.btnRemoveFilters.Click += new System.EventHandler(this.btnRemoveFilters_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(1117, 562);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(120, 62);
            this.btnLogOut.TabIndex = 39;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lbCustomer
            // 
            this.lbCustomer.FormattingEnabled = true;
            this.lbCustomer.ItemHeight = 16;
            this.lbCustomer.Location = new System.Drawing.Point(384, 393);
            this.lbCustomer.Name = "lbCustomer";
            this.lbCustomer.Size = new System.Drawing.Size(135, 100);
            this.lbCustomer.TabIndex = 40;
            // 
            // lbBorrowDate
            // 
            this.lbBorrowDate.FormattingEnabled = true;
            this.lbBorrowDate.ItemHeight = 16;
            this.lbBorrowDate.Location = new System.Drawing.Point(590, 393);
            this.lbBorrowDate.Name = "lbBorrowDate";
            this.lbBorrowDate.Size = new System.Drawing.Size(135, 100);
            this.lbBorrowDate.TabIndex = 41;
            // 
            // lbReturnDate
            // 
            this.lbReturnDate.FormattingEnabled = true;
            this.lbReturnDate.ItemHeight = 16;
            this.lbReturnDate.Location = new System.Drawing.Point(790, 393);
            this.lbReturnDate.Name = "lbReturnDate";
            this.lbReturnDate.Size = new System.Drawing.Size(135, 100);
            this.lbReturnDate.TabIndex = 42;
            // 
            // tbxCopies
            // 
            this.tbxCopies.Location = new System.Drawing.Point(429, 257);
            this.tbxCopies.Multiline = true;
            this.tbxCopies.Name = "tbxCopies";
            this.tbxCopies.ReadOnly = true;
            this.tbxCopies.Size = new System.Drawing.Size(209, 33);
            this.tbxCopies.TabIndex = 43;
            // 
            // lblCopies
            // 
            this.lblCopies.AutoSize = true;
            this.lblCopies.Location = new System.Drawing.Point(429, 232);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(50, 16);
            this.lblCopies.TabIndex = 44;
            this.lblCopies.Text = "Copies";
            // 
            // lbCurrentBorrowers
            // 
            this.lbCurrentBorrowers.FormattingEnabled = true;
            this.lbCurrentBorrowers.ItemHeight = 16;
            this.lbCurrentBorrowers.Location = new System.Drawing.Point(684, 193);
            this.lbCurrentBorrowers.Name = "lbCurrentBorrowers";
            this.lbCurrentBorrowers.Size = new System.Drawing.Size(190, 100);
            this.lbCurrentBorrowers.TabIndex = 45;
            // 
            // btnSaveData
            // 
            this.btnSaveData.Location = new System.Drawing.Point(975, 522);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(124, 102);
            this.btnSaveData.TabIndex = 46;
            this.btnSaveData.Text = "Save Data";
            this.btnSaveData.UseVisualStyleBackColor = true;
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // LibraryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 636);
            this.Controls.Add(this.btnSaveData);
            this.Controls.Add(this.lbCurrentBorrowers);
            this.Controls.Add(this.lblCopies);
            this.Controls.Add(this.tbxCopies);
            this.Controls.Add(this.lbReturnDate);
            this.Controls.Add(this.lbBorrowDate);
            this.Controls.Add(this.lbCustomer);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnRemoveFilters);
            this.Controls.Add(this.lblFilters);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.tbxFilterAuthor);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.cmbGenres);
            this.Controls.Add(this.lblFilterTitle);
            this.Controls.Add(this.lblFilterAuthor);
            this.Controls.Add(this.lblFilterGenre);
            this.Controls.Add(this.btnShowBooks);
            this.Controls.Add(this.btnReturnBook);
            this.Controls.Add(this.btnBorrowBook);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.lblGenre);
            this.Controls.Add(this.lblPublicationDate);
            this.Controls.Add(this.lblPages);
            this.Controls.Add(this.lblIsbn13);
            this.Controls.Add(this.lblCurrentBorrower);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblHistoryOfBorrowers);
            this.Controls.Add(this.lblPerson);
            this.Controls.Add(this.lblBorrowDate);
            this.Controls.Add(this.lblReturnDate);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxGenre);
            this.Controls.Add(this.tbxPublicationDate);
            this.Controls.Add(this.tbxPages);
            this.Controls.Add(this.tbxIsbn13);
            this.Controls.Add(this.tbxDescription);
            this.Controls.Add(this.lbBookTitle);
            this.Name = "LibraryForm";
            this.Text = "LibraryForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lbBookTitle;
        private System.Windows.Forms.TextBox tbxDescription;
        private System.Windows.Forms.TextBox tbxIsbn13;
        private System.Windows.Forms.TextBox tbxPages;
        private System.Windows.Forms.TextBox tbxPublicationDate;
        private System.Windows.Forms.TextBox tbxGenre;
        private System.Windows.Forms.TextBox tbxAuthor;
        private System.Windows.Forms.Label lblReturnDate;
        private System.Windows.Forms.Label lblBorrowDate;
        private System.Windows.Forms.Label lblPerson;
        private System.Windows.Forms.Label lblHistoryOfBorrowers;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblCurrentBorrower;
        private System.Windows.Forms.Label lblIsbn13;
        private System.Windows.Forms.Label lblPages;
        private System.Windows.Forms.Label lblPublicationDate;
        private System.Windows.Forms.Label lblGenre;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnBorrowBook;
        private System.Windows.Forms.Button btnReturnBook;
        private System.Windows.Forms.Button btnShowBooks;
        private System.Windows.Forms.Label lblFilterGenre;
        private System.Windows.Forms.Label lblFilterAuthor;
        private System.Windows.Forms.Label lblFilterTitle;
        private System.Windows.Forms.ComboBox cmbGenres;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.TextBox tbxFilterAuthor;
        private System.Windows.Forms.TextBox tbxTitle;
        private System.Windows.Forms.Label lblFilters;
        private System.Windows.Forms.Button btnRemoveFilters;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.ListBox lbCustomer;
        private System.Windows.Forms.ListBox lbBorrowDate;
        private System.Windows.Forms.ListBox lbReturnDate;
        private System.Windows.Forms.TextBox tbxCopies;
        private System.Windows.Forms.Label lblCopies;
        private System.Windows.Forms.ListBox lbCurrentBorrowers;
        private System.Windows.Forms.Button btnSaveData;
    }
}