﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public class Employee
    {
        private string name;
        private string password;
        private bool isAdmin = false;
        private bool isLoggedIn = false;

        public Employee(string name, string password)
        {
            this.name = name;
            this.password = password;
            isAdmin = true;
            isLoggedIn = false;
        }

        public string Name { get { return name; } }
        public string Password { get { return password; } }
        public bool IsLoggedIn { get { return isLoggedIn; } set { isLoggedIn = value; } }
        public bool IsAdmin { get { return isAdmin; } }
    }
}
