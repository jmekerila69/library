﻿namespace Library_Individual_Assignment
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAuthor = new System.Windows.Forms.Label();
            this.lblGenre = new System.Windows.Forms.Label();
            this.lblPublicationDate = new System.Windows.Forms.Label();
            this.lblPages = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tbxAuthor = new System.Windows.Forms.TextBox();
            this.tbxPages = new System.Windows.Forms.TextBox();
            this.tbxTitle = new System.Windows.Forms.TextBox();
            this.tbxDescription = new System.Windows.Forms.TextBox();
            this.cmbGenre = new System.Windows.Forms.ComboBox();
            this.dtpPublicationDate = new System.Windows.Forms.DateTimePicker();
            this.lblDescriptionOfBook = new System.Windows.Forms.Label();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.lbTitles = new System.Windows.Forms.ListBox();
            this.btnRemoveBook = new System.Windows.Forms.Button();
            this.lblAllBooks = new System.Windows.Forms.Label();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.tbxCopies = new System.Windows.Forms.TextBox();
            this.lblCopies = new System.Windows.Forms.Label();
            this.lblFilters = new System.Windows.Forms.Label();
            this.tbxFilterTitle = new System.Windows.Forms.TextBox();
            this.tbxFilterAuthor = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.cmbGenres = new System.Windows.Forms.ComboBox();
            this.lblFilterTitle = new System.Windows.Forms.Label();
            this.lblFilterAuthor = new System.Windows.Forms.Label();
            this.lblFilterGenre = new System.Windows.Forms.Label();
            this.btnAddMoreCopies = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tbxNumberOfCopies = new System.Windows.Forms.TextBox();
            this.tbxShowAllBooks = new System.Windows.Forms.Button();
            this.tbxShowAuthor = new System.Windows.Forms.TextBox();
            this.tbxShowGenre = new System.Windows.Forms.TextBox();
            this.tbxShowCopies = new System.Windows.Forms.TextBox();
            this.lblAuthorForShowing = new System.Windows.Forms.Label();
            this.lblGenreForShowing = new System.Windows.Forms.Label();
            this.lblCopiesForShowing = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(264, 21);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(45, 16);
            this.lblAuthor.TabIndex = 39;
            this.lblAuthor.Text = "Author";
            // 
            // lblGenre
            // 
            this.lblGenre.AutoSize = true;
            this.lblGenre.Location = new System.Drawing.Point(9, 88);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(44, 16);
            this.lblGenre.TabIndex = 38;
            this.lblGenre.Text = "Genre";
            // 
            // lblPublicationDate
            // 
            this.lblPublicationDate.AutoSize = true;
            this.lblPublicationDate.Location = new System.Drawing.Point(9, 166);
            this.lblPublicationDate.Name = "lblPublicationDate";
            this.lblPublicationDate.Size = new System.Drawing.Size(105, 16);
            this.lblPublicationDate.TabIndex = 37;
            this.lblPublicationDate.Text = "Publication Date";
            // 
            // lblPages
            // 
            this.lblPages.AutoSize = true;
            this.lblPages.Location = new System.Drawing.Point(264, 95);
            this.lblPages.Name = "lblPages";
            this.lblPages.Size = new System.Drawing.Size(47, 16);
            this.lblPages.TabIndex = 36;
            this.lblPages.Text = "Pages";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(9, 27);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(33, 16);
            this.lblTitle.TabIndex = 35;
            this.lblTitle.Text = "Title";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(524, -17);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(75, 16);
            this.lblDescription.TabIndex = 33;
            this.lblDescription.Text = "Description";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(267, 46);
            this.tbxAuthor.Multiline = true;
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(190, 35);
            this.tbxAuthor.TabIndex = 32;
            // 
            // tbxPages
            // 
            this.tbxPages.Location = new System.Drawing.Point(267, 114);
            this.tbxPages.Multiline = true;
            this.tbxPages.Name = "tbxPages";
            this.tbxPages.Size = new System.Drawing.Size(190, 24);
            this.tbxPages.TabIndex = 29;
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(12, 46);
            this.tbxTitle.Multiline = true;
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(209, 33);
            this.tbxTitle.TabIndex = 28;
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(523, 27);
            this.tbxDescription.Multiline = true;
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.Size = new System.Drawing.Size(265, 191);
            this.tbxDescription.TabIndex = 27;
            // 
            // cmbGenre
            // 
            this.cmbGenre.FormattingEnabled = true;
            this.cmbGenre.Location = new System.Drawing.Point(12, 114);
            this.cmbGenre.Name = "cmbGenre";
            this.cmbGenre.Size = new System.Drawing.Size(209, 24);
            this.cmbGenre.TabIndex = 40;
            // 
            // dtpPublicationDate
            // 
            this.dtpPublicationDate.Location = new System.Drawing.Point(12, 196);
            this.dtpPublicationDate.Name = "dtpPublicationDate";
            this.dtpPublicationDate.Size = new System.Drawing.Size(200, 22);
            this.dtpPublicationDate.TabIndex = 41;
            // 
            // lblDescriptionOfBook
            // 
            this.lblDescriptionOfBook.AutoSize = true;
            this.lblDescriptionOfBook.Location = new System.Drawing.Point(523, 5);
            this.lblDescriptionOfBook.Name = "lblDescriptionOfBook";
            this.lblDescriptionOfBook.Size = new System.Drawing.Size(75, 16);
            this.lblDescriptionOfBook.TabIndex = 42;
            this.lblDescriptionOfBook.Text = "Description";
            // 
            // btnAddBook
            // 
            this.btnAddBook.Location = new System.Drawing.Point(12, 260);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(190, 35);
            this.btnAddBook.TabIndex = 43;
            this.btnAddBook.Text = "Add Book";
            this.btnAddBook.UseVisualStyleBackColor = true;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // lbTitles
            // 
            this.lbTitles.FormattingEnabled = true;
            this.lbTitles.ItemHeight = 16;
            this.lbTitles.Location = new System.Drawing.Point(217, 288);
            this.lbTitles.Name = "lbTitles";
            this.lbTitles.Size = new System.Drawing.Size(261, 228);
            this.lbTitles.TabIndex = 44;
            this.lbTitles.SelectedIndexChanged += new System.EventHandler(this.lbTitles_SelectedIndexChanged);
            // 
            // btnRemoveBook
            // 
            this.btnRemoveBook.Location = new System.Drawing.Point(12, 314);
            this.btnRemoveBook.Name = "btnRemoveBook";
            this.btnRemoveBook.Size = new System.Drawing.Size(190, 37);
            this.btnRemoveBook.TabIndex = 45;
            this.btnRemoveBook.Text = "Remove Book";
            this.btnRemoveBook.UseVisualStyleBackColor = true;
            this.btnRemoveBook.Click += new System.EventHandler(this.btnRemoveBook_Click);
            // 
            // lblAllBooks
            // 
            this.lblAllBooks.AutoSize = true;
            this.lblAllBooks.Location = new System.Drawing.Point(214, 260);
            this.lblAllBooks.Name = "lblAllBooks";
            this.lblAllBooks.Size = new System.Drawing.Size(64, 16);
            this.lblAllBooks.TabIndex = 46;
            this.lblAllBooks.Text = "All Books";
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(12, 465);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(134, 51);
            this.btnLogOut.TabIndex = 47;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // tbxCopies
            // 
            this.tbxCopies.Location = new System.Drawing.Point(267, 196);
            this.tbxCopies.Name = "tbxCopies";
            this.tbxCopies.Size = new System.Drawing.Size(190, 22);
            this.tbxCopies.TabIndex = 48;
            // 
            // lblCopies
            // 
            this.lblCopies.AutoSize = true;
            this.lblCopies.Location = new System.Drawing.Point(267, 165);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(50, 16);
            this.lblCopies.TabIndex = 49;
            this.lblCopies.Text = "Copies";
            // 
            // lblFilters
            // 
            this.lblFilters.AutoSize = true;
            this.lblFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilters.Location = new System.Drawing.Point(656, 251);
            this.lblFilters.Name = "lblFilters";
            this.lblFilters.Size = new System.Drawing.Size(64, 25);
            this.lblFilters.TabIndex = 57;
            this.lblFilters.Text = "Filters";
            // 
            // tbxFilterTitle
            // 
            this.tbxFilterTitle.Location = new System.Drawing.Point(668, 319);
            this.tbxFilterTitle.Name = "tbxFilterTitle";
            this.tbxFilterTitle.Size = new System.Drawing.Size(100, 22);
            this.tbxFilterTitle.TabIndex = 56;
            // 
            // tbxFilterAuthor
            // 
            this.tbxFilterAuthor.Location = new System.Drawing.Point(668, 389);
            this.tbxFilterAuthor.Name = "tbxFilterAuthor";
            this.tbxFilterAuthor.Size = new System.Drawing.Size(120, 22);
            this.tbxFilterAuthor.TabIndex = 55;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(672, 482);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(116, 34);
            this.btnFilter.TabIndex = 54;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // cmbGenres
            // 
            this.cmbGenres.FormattingEnabled = true;
            this.cmbGenres.Location = new System.Drawing.Point(668, 454);
            this.cmbGenres.Name = "cmbGenres";
            this.cmbGenres.Size = new System.Drawing.Size(121, 24);
            this.cmbGenres.TabIndex = 53;
            // 
            // lblFilterTitle
            // 
            this.lblFilterTitle.AutoSize = true;
            this.lblFilterTitle.Location = new System.Drawing.Point(665, 290);
            this.lblFilterTitle.Name = "lblFilterTitle";
            this.lblFilterTitle.Size = new System.Drawing.Size(33, 16);
            this.lblFilterTitle.TabIndex = 52;
            this.lblFilterTitle.Text = "Title";
            // 
            // lblFilterAuthor
            // 
            this.lblFilterAuthor.AutoSize = true;
            this.lblFilterAuthor.Location = new System.Drawing.Point(669, 360);
            this.lblFilterAuthor.Name = "lblFilterAuthor";
            this.lblFilterAuthor.Size = new System.Drawing.Size(45, 16);
            this.lblFilterAuthor.TabIndex = 51;
            this.lblFilterAuthor.Text = "Author";
            // 
            // lblFilterGenre
            // 
            this.lblFilterGenre.AutoSize = true;
            this.lblFilterGenre.Location = new System.Drawing.Point(676, 425);
            this.lblFilterGenre.Name = "lblFilterGenre";
            this.lblFilterGenre.Size = new System.Drawing.Size(44, 16);
            this.lblFilterGenre.TabIndex = 50;
            this.lblFilterGenre.Text = "Genre";
            // 
            // btnAddMoreCopies
            // 
            this.btnAddMoreCopies.Location = new System.Drawing.Point(12, 372);
            this.btnAddMoreCopies.Name = "btnAddMoreCopies";
            this.btnAddMoreCopies.Size = new System.Drawing.Size(190, 39);
            this.btnAddMoreCopies.TabIndex = 58;
            this.btnAddMoreCopies.Text = "Add more copies";
            this.btnAddMoreCopies.UseVisualStyleBackColor = true;
            this.btnAddMoreCopies.Click += new System.EventHandler(this.btnAddMoreCopies_Click);
            // 
            // tbxNumberOfCopies
            // 
            this.tbxNumberOfCopies.Location = new System.Drawing.Point(13, 424);
            this.tbxNumberOfCopies.Name = "tbxNumberOfCopies";
            this.tbxNumberOfCopies.Size = new System.Drawing.Size(175, 22);
            this.tbxNumberOfCopies.TabIndex = 59;
            // 
            // tbxShowAllBooks
            // 
            this.tbxShowAllBooks.Location = new System.Drawing.Point(514, 483);
            this.tbxShowAllBooks.Name = "tbxShowAllBooks";
            this.tbxShowAllBooks.Size = new System.Drawing.Size(125, 33);
            this.tbxShowAllBooks.TabIndex = 60;
            this.tbxShowAllBooks.Text = "Show all books";
            this.tbxShowAllBooks.UseVisualStyleBackColor = true;
            this.tbxShowAllBooks.Click += new System.EventHandler(this.tbxShowAllBooks_Click);
            // 
            // tbxShowAuthor
            // 
            this.tbxShowAuthor.Location = new System.Drawing.Point(498, 303);
            this.tbxShowAuthor.Multiline = true;
            this.tbxShowAuthor.Name = "tbxShowAuthor";
            this.tbxShowAuthor.ReadOnly = true;
            this.tbxShowAuthor.Size = new System.Drawing.Size(103, 44);
            this.tbxShowAuthor.TabIndex = 61;
            // 
            // tbxShowGenre
            // 
            this.tbxShowGenre.Location = new System.Drawing.Point(498, 372);
            this.tbxShowGenre.Multiline = true;
            this.tbxShowGenre.Name = "tbxShowGenre";
            this.tbxShowGenre.ReadOnly = true;
            this.tbxShowGenre.Size = new System.Drawing.Size(103, 32);
            this.tbxShowGenre.TabIndex = 62;
            // 
            // tbxShowCopies
            // 
            this.tbxShowCopies.Location = new System.Drawing.Point(501, 441);
            this.tbxShowCopies.Multiline = true;
            this.tbxShowCopies.Name = "tbxShowCopies";
            this.tbxShowCopies.ReadOnly = true;
            this.tbxShowCopies.Size = new System.Drawing.Size(100, 27);
            this.tbxShowCopies.TabIndex = 63;
            // 
            // lblAuthorForShowing
            // 
            this.lblAuthorForShowing.AutoSize = true;
            this.lblAuthorForShowing.Location = new System.Drawing.Point(498, 278);
            this.lblAuthorForShowing.Name = "lblAuthorForShowing";
            this.lblAuthorForShowing.Size = new System.Drawing.Size(45, 16);
            this.lblAuthorForShowing.TabIndex = 64;
            this.lblAuthorForShowing.Text = "Author";
            // 
            // lblGenreForShowing
            // 
            this.lblGenreForShowing.AutoSize = true;
            this.lblGenreForShowing.Location = new System.Drawing.Point(498, 350);
            this.lblGenreForShowing.Name = "lblGenreForShowing";
            this.lblGenreForShowing.Size = new System.Drawing.Size(44, 16);
            this.lblGenreForShowing.TabIndex = 65;
            this.lblGenreForShowing.Text = "Genre";
            // 
            // lblCopiesForShowing
            // 
            this.lblCopiesForShowing.AutoSize = true;
            this.lblCopiesForShowing.Location = new System.Drawing.Point(501, 419);
            this.lblCopiesForShowing.Name = "lblCopiesForShowing";
            this.lblCopiesForShowing.Size = new System.Drawing.Size(50, 16);
            this.lblCopiesForShowing.TabIndex = 66;
            this.lblCopiesForShowing.Text = "Copies";
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 528);
            this.Controls.Add(this.lblCopiesForShowing);
            this.Controls.Add(this.lblGenreForShowing);
            this.Controls.Add(this.lblAuthorForShowing);
            this.Controls.Add(this.tbxShowCopies);
            this.Controls.Add(this.tbxShowGenre);
            this.Controls.Add(this.tbxShowAuthor);
            this.Controls.Add(this.tbxShowAllBooks);
            this.Controls.Add(this.tbxNumberOfCopies);
            this.Controls.Add(this.btnAddMoreCopies);
            this.Controls.Add(this.lblFilters);
            this.Controls.Add(this.tbxFilterTitle);
            this.Controls.Add(this.tbxFilterAuthor);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.cmbGenres);
            this.Controls.Add(this.lblFilterTitle);
            this.Controls.Add(this.lblFilterAuthor);
            this.Controls.Add(this.lblFilterGenre);
            this.Controls.Add(this.lblCopies);
            this.Controls.Add(this.tbxCopies);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.lblAllBooks);
            this.Controls.Add(this.btnRemoveBook);
            this.Controls.Add(this.lbTitles);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.lblDescriptionOfBook);
            this.Controls.Add(this.dtpPublicationDate);
            this.Controls.Add(this.cmbGenre);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.lblGenre);
            this.Controls.Add(this.lblPublicationDate);
            this.Controls.Add(this.lblPages);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxPages);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.tbxDescription);
            this.Name = "EmployeeForm";
            this.Text = "ManagerForm";
            this.Load += new System.EventHandler(this.ManagerForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label lblGenre;
        private System.Windows.Forms.Label lblPublicationDate;
        private System.Windows.Forms.Label lblPages;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox tbxAuthor;
        private System.Windows.Forms.TextBox tbxPages;
        private System.Windows.Forms.TextBox tbxTitle;
        private System.Windows.Forms.TextBox tbxDescription;
        private System.Windows.Forms.ComboBox cmbGenre;
        private System.Windows.Forms.DateTimePicker dtpPublicationDate;
        private System.Windows.Forms.Label lblDescriptionOfBook;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.ListBox lbTitles;
        private System.Windows.Forms.Button btnRemoveBook;
        private System.Windows.Forms.Label lblAllBooks;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.TextBox tbxCopies;
        private System.Windows.Forms.Label lblCopies;
        private System.Windows.Forms.Label lblFilters;
        private System.Windows.Forms.TextBox tbxFilterTitle;
        private System.Windows.Forms.TextBox tbxFilterAuthor;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ComboBox cmbGenres;
        private System.Windows.Forms.Label lblFilterTitle;
        private System.Windows.Forms.Label lblFilterAuthor;
        private System.Windows.Forms.Label lblFilterGenre;
        private System.Windows.Forms.Button btnAddMoreCopies;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox tbxNumberOfCopies;
        private System.Windows.Forms.Button tbxShowAllBooks;
        private System.Windows.Forms.TextBox tbxShowAuthor;
        private System.Windows.Forms.TextBox tbxShowGenre;
        private System.Windows.Forms.TextBox tbxShowCopies;
        private System.Windows.Forms.Label lblAuthorForShowing;
        private System.Windows.Forms.Label lblGenreForShowing;
        private System.Windows.Forms.Label lblCopiesForShowing;
    }
}