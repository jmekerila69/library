﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public class Library
    {
        private string name;
        private List<Customer> customers;
        private List<Book> books;
        private List<Employee> employees;
        private static int customerIdSeeder = 00001;
        private int customerId;
        FileStream fs;
        BinaryFormatter bf = null;

        public Library(string name)
        {
            this.name = name;
            this.customers = new List<Customer>();
            this.books = new List<Book>();
            this.employees = new List<Employee>();
            this.customerId = customerIdSeeder;

            if (new FileInfo("Books.txt").Length != 0)
            {
                try
                {
                    fs = new FileStream("Books.txt", FileMode.Open, FileAccess.Read);
                    bf = new BinaryFormatter();
                    this.books = (List<Book>)bf.Deserialize(fs);
                }
                catch (Exception ex) { System.Windows.Forms.MessageBox.Show(ex.ToString()); }
                finally { if (fs != null) fs.Close(); }
            }

            if (new FileInfo("Customers.txt").Length != 0)
            {
                try
                {
                    fs = new FileStream("Customers.txt", FileMode.Open, FileAccess.Read);
                    bf = new BinaryFormatter();
                    this.customers = (List<Customer>)bf.Deserialize(fs);

                    foreach (Customer c in GetCustomers().ToArray())
                    {
                        foreach (Book b in c.GetBorrowedBooks().ToArray())
                        {
                            if (b.Isbn13 == GetBook(b.Title).Isbn13)
                            {
                                c.BorrowedBooks.Remove(b);
                                c.BorrowedBooks.Add(GetBook(b.Title));
                            }
                        }
                    }
                }
                catch (Exception ex) { System.Windows.Forms.MessageBox.Show(ex.ToString()); }
                finally { if (fs != null) fs.Close(); }
            }

            if (new FileInfo("Employees.txt").Length != 0)
            {
                try
                {
                    fs = new FileStream("Employees.txt", FileMode.Open, FileAccess.Read);
                    bf = new BinaryFormatter();
                    this.employees = (List<Employee>)bf.Deserialize(fs);

                }
                catch (Exception ex) { System.Windows.Forms.MessageBox.Show(ex.ToString()); }
                finally { if (fs != null) fs.Close(); }
            }


            foreach (Customer c in customers)
            {
                c.Id += 1;
            }
        }

        public void AddBook(string ISNB13, string title, string author, GenreType genre, DateTime publicationDate, int pages, string description, int copies)
        {
            books.Add(new Book(ISNB13, title, author, genre, publicationDate, pages, description, copies));
        }

        public Book[] FilterBooks(string title, string author, GenreType genre)
        {

            List<Book> filteredBooks = new List<Book>();

            foreach (Book b in books)
            {
                if (title != "" && author != "" && Convert.ToInt32(genre) != -1)
                {
                    if (genre == b.Genre && b.Title.Contains(title) && author == b.Author)
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title != "" && author != "" && Convert.ToInt32(genre) == -1)
                {
                    if (b.Title.Contains(title) && author == b.Author)
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title != "" && author == "" && Convert.ToInt32(genre) != -1)
                {
                    if (b.Title.Contains(title) && genre == b.Genre)
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title == "" && author != "" && Convert.ToInt32(genre) != -1)
                {
                    if (genre == b.Genre && author == b.Author)
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title != "" && author == "" && Convert.ToInt32(genre) == -1)
                {
                    if (b.Title.Contains(title))
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title == "" && author != "" && Convert.ToInt32(genre) == -1)
                {
                    if (author == b.Author)
                    {
                        filteredBooks.Add(b);
                    }
                }
                else if (title == "" && author == "" && Convert.ToInt32(genre) != -1)
                {
                    if (genre == b.Genre)
                    {
                        filteredBooks.Add(b);
                    }
                }
            }
            return filteredBooks.ToArray();
        }
        public Book GetBook(string title)
        {
            foreach (Book book in books)
            {
                if (title == book.Title)
                {
                    return book;
                }
            }
            return null;
        }
        public Book[] GetBooks()
        {
            return books.ToArray();
        }

        public void RemoveBook(Book b)
        {
            books.Remove(b);
        }

        public void AddCustomer(string name, string password)
        {
            customers.Add(new Customer(name, password));
            this.customerId++;
        }

        public Customer[] GetCustomers()
        {
            return customers.ToArray();
        }

        public Customer GetCustomer(string name, string password)
        {
            foreach (Customer c in customers)
            {
                if (name == c.Name && password == c.Password)
                {
                    c.IsLoggedIn = true;
                    return c;
                }
            }
            return null;
        }

        public Customer GetCustomer()
        {
            foreach (Customer c in customers)
            {
                if (c.IsLoggedIn == true)
                {
                    return c;
                }
            }
            return null;
        }

        public void AddEmployee(string name, string password)
        {
            employees.Add(new Employee(name, password));
        }

        public Employee GetEmployee(string name, string password)
        {
            foreach (Employee emp in employees)
            {
                if (name == emp.Name && password == emp.Password)
                {
                    emp.IsLoggedIn = true;
                    return emp;
                }
            }
            return null;
        }

        public Employee GetEmployee()
        {
            foreach (Employee emp in employees)
            {
                if (emp.IsLoggedIn == true)
                {
                    return emp;
                }
            }
            return null;
        }

        public Employee[] GetEmployees()
        {
            return employees.ToArray();
        }
    }
}
