﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public partial class MainPage : Form
    {
        Library library;
        MainPage mainPage;
        LogInForm logInForm;
        SignUpForm signUpForm;
        LibraryForm libraryForm;
        public MainPage()
        {
            InitializeComponent();
            library = new Library("Bogdans library");
            //mainPage = new MainPage();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            mainPage = new MainPage();
            logInForm = new LogInForm(library);
            logInForm.Show();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            mainPage = new MainPage();
            signUpForm = new SignUpForm(library);
            signUpForm.Show();
        }

        private void btnSeeAllBooks_Click(object sender, EventArgs e)
        {
            mainPage = new MainPage();
            libraryForm = new LibraryForm(library);
            libraryForm.Show();
            mainPage.Close();
        }

        private void MainPage_Load(object sender, EventArgs e)
        {

        }

        private void lblLibraryName_Click(object sender, EventArgs e)
        {

        }
    }
}
