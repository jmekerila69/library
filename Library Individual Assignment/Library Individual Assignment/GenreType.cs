﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public enum GenreType
    {
        Fantasy,
        Science_Fiction,
        Action_Adventure,
        Mystery,
        Horror,
        Thriller,
        Romance,
        Short_Story,
        Children,
        Biography,
        History,
        Travel,
        True_Crime,
        Humor
    }
}
