﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public class ReturnData
    {
        private string borrower;
        private DateTime borrowedDate;
        private DateTime returnDate;

        public ReturnData(string borrower, DateTime borrowedDate, DateTime returnDate)
        {
            this.borrower = borrower;
            this.borrowedDate = borrowedDate;
            this.returnDate = returnDate;
        }

        public string Borrower { get { return borrower; } set { borrower = value; } }
        public DateTime BorrowedDate { get { return borrowedDate; }}
        public DateTime ReturnDate { get { return returnDate; }}
    }
}
