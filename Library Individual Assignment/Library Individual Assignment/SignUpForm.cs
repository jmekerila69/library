﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public partial class SignUpForm : Form
    {
        Library l;
        public SignUpForm(Library library)
        {
            InitializeComponent();
            l = library;
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbxName.Text) && !String.IsNullOrEmpty(tbxPassword.Text))
            {
                if (MessageBox.Show("Are you an employee", "Employee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (!l.GetEmployees().Any(c => c.Name == tbxName.Text) && !l.GetCustomers().Any(c => c.Name == tbxName.Text))
                    {
                        l.AddEmployee(tbxName.Text, tbxPassword.Text);
                        MessageBox.Show("Employee account created succesfully");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Username already exists");
                    }

                }
                else
                {
                    if (!l.GetEmployees().Any(c => c.Name == tbxName.Text) && !l.GetCustomers().Any(c => c.Name == tbxName.Text))
                    {
                        l.AddCustomer(tbxName.Text, tbxPassword.Text);
                        MessageBox.Show("Customer account created succesfully");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Username already exists");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please provide a name and a password");
            }
        }
    }
}
