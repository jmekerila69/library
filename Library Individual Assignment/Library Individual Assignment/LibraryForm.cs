﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Library_Individual_Assignment
{
    [Serializable]
    public partial class LibraryForm : Form
    {
        Library l;
        FileStream fs;
        BinaryFormatter bf = null;
        public LibraryForm(Library library)
        {
            InitializeComponent();
            lbBookTitle.Items.Clear();

            l = library;

            if (l.GetCustomer() == null)
            {
                btnShowBooks.Visible = false;
                btnBorrowBook.Visible = false;
                btnReturnBook.Visible = false;
                btnLogOut.Visible = false;
            }

            foreach (GenreType genre in Enum.GetValues(typeof(GenreType)))
            {
                cmbGenres.Items.Add(genre);
            }

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbBookTitle.Items.Add(b);
                }
            }
        }

        private void lbBookTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbBookTitle.SelectedIndex > -1)
            {
                lbCustomer.Items.Clear();
                lbBorrowDate.Items.Clear();
                lbReturnDate.Items.Clear();
                lbCurrentBorrowers.Items.Clear();

                Book selectedBook = (Book)lbBookTitle.SelectedItem;

                tbxAuthor.Text = selectedBook.Author;
                tbxDescription.Text = selectedBook.Description;
                tbxGenre.Text = selectedBook.Genre.ToString();
                tbxPages.Text = selectedBook.Pages.ToString();
                tbxPublicationDate.Text = Convert.ToString($"{selectedBook.PublicationDate.Date:dd/MM/yyyy}");
                tbxIsbn13.Text = selectedBook.Isbn13.ToString();

                foreach (string s in selectedBook.GetBorrowers())
                {
                    lbCurrentBorrowers.Items.Add(s);
                }
                tbxCopies.Text = selectedBook.Copies.ToString();

                foreach (ReturnData r in selectedBook.GetAllReturnData())
                {
                    if (selectedBook.GetAllReturnData().Length > 0)
                    {
                        if (r.ReturnDate != null && r.BorrowedDate != null)
                        {
                            lbCustomer.Items.Add(r.Borrower);
                            lbBorrowDate.Items.Add(r.BorrowedDate.ToString("dd/MM/yyyy"));
                            lbReturnDate.Items.Add(r.ReturnDate.ToString("dd/MM/yyyy"));
                        }
                    }
                }
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            lbBookTitle.Items.Clear();

            foreach (Book b in l.FilterBooks(tbxTitle.Text, tbxFilterAuthor.Text, (GenreType)cmbGenres.SelectedIndex))
            {
                lbBookTitle.Items.Add(b);
            }
            tbxFilterAuthor.Clear();
            tbxTitle.Clear();
            cmbGenres.SelectedIndex = -1;
        }

        private void btnRemoveFilters_Click(object sender, EventArgs e)
        {
            lbBookTitle.Items.Clear();

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbBookTitle.Items.Add(b);
                }
            }
            lbBookTitle.Enabled = true;
        }

        private void btnBorrowBook_Click(object sender, EventArgs e)
        {
            if (l.GetCustomer() != null)
            {
                if (lbBookTitle.SelectedIndex > -1)
                {
                    Book selectedBook = (Book)lbBookTitle.SelectedItem;
                    if (!l.GetCustomer().GetBorrowedBooks().Contains(selectedBook))
                    {
                        l.GetCustomer().BorrowBook(selectedBook);
                    }
                    else
                    {
                        MessageBox.Show("You have already borrowed this book");
                    }
                }

            }

            lbBookTitle.Items.Clear();

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbBookTitle.Items.Add(b);
                }
            }
        }

        private void btnReturnBook_Click(object sender, EventArgs e)
        {
            if (l.GetCustomer() != null)
            {
                Book selectedBook = (Book)lbBookTitle.SelectedItem;

                if (lbBookTitle.SelectedIndex > -1)
                {
                    if (l.GetCustomer().GetBorrowedBooks().Contains(selectedBook))
                    //if (Has(l.GetCustomer().GetBorrowedBooks(), selectedBook))
                    {
                        l.GetCustomer().ReturnBook(selectedBook);

                        selectedBook.PastBorrower = l.GetCustomer().GetInfo();

                        selectedBook.AddReturnData(selectedBook.PastBorrower, selectedBook.BorrowedDate, selectedBook.ReturnDate);

                        selectedBook.RemoveBorrowers(l.GetCustomer().ToString());
                    }
                    else
                    {
                        MessageBox.Show("You have not borrowed this book");
                    }
                }
                else
                {
                    MessageBox.Show("Nothing was selected");
                }

            }

            lbBookTitle.Items.Clear();

            if (l.GetBooks() != null)
            {
                foreach (Book b in l.GetBooks())
                {
                    lbBookTitle.Items.Add(b);
                }
            }
        }

        private bool Has(Book[] books, Book selectedBook)
        {
            foreach (Book book in books)
            {
                if (book.Isbn13==selectedBook.Isbn13 && book.Author == selectedBook.Author)
                {
                    MessageBox.Show("OK");
                    return true;
                } else
                { MessageBox.Show("nOT"); }
            }
            return false;
        }

        private void btnShowBooks_Click(object sender, EventArgs e)
        {
            lbBookTitle.Items.Clear();

            if (l.GetCustomer() != null)
            {
                foreach (Book b in l.GetCustomer().GetBorrowedBooks())
                {
                    lbBookTitle.Items.Add(b);
                }
            }
            //lbBookTitle.Enabled = false;

            tbxAuthor.Clear();
            tbxDescription.Clear();
            tbxGenre.Clear();
            tbxPages.Clear();
            tbxPublicationDate.Clear();
            tbxIsbn13.Clear();

        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            if (l.GetCustomer() != null)
            {
                foreach (Customer c in l.GetCustomers())
                {
                    c.IsLoggedIn = false;
                }
                this.Close();
            }
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                fs = new FileStream("Books.txt", FileMode.Open, FileAccess.Write);
                bf = new BinaryFormatter();
                bf.Serialize(fs, l.GetBooks().ToList());
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { if (fs != null) fs.Close(); }

            try
            {
                fs = new FileStream("Customers.txt", FileMode.Open, FileAccess.Write);
                bf = new BinaryFormatter();
                bf.Serialize(fs, l.GetCustomers().ToList());
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { if (fs != null) fs.Close(); }

            try
            {
                fs = new FileStream("Employees.txt", FileMode.Open, FileAccess.Write);
                bf = new BinaryFormatter();
                bf.Serialize(fs, l.GetEmployees().ToList());
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { if (fs != null) fs.Close(); }

        }
    }
}
